from django.apps import AppConfig


class AutoDevOpsConfig(AppConfig):
    name = 'autodevops'
